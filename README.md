# sec_ws1920

> Homework tasks of the "Introduction to IT Security" course at the Technische
> Universität Braunschweig, Germany.

## Licence

[0BSD](https://spdx.org/licenses/0BSD.html) © 2019–2020 Nikita Karamov
